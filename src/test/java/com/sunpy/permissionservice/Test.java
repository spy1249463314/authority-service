package com.sunpy.permissionservice;

import com.sunpy.permissionservice.bo.UserBO;
import com.sunpy.permissionservice.po.User;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static <S, T> void copyBean(S source, T target) {
        BeanUtils.copyProperties(source, target);
    }

    public static <S, T> List<T> copyList(List<S> sourceList, Class<T> clazz) {
        return sourceList.stream().map(source -> {
            T t = null;

            try {
                t = clazz.newInstance();
                copyBean(source, t);

            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }

            return t;
        }).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<User> userList = new ArrayList<>();

        for (int i = 0 ; i < 10 ; i++) {
            User user = new User();
            user.setUserId((long) i);
            user.setUserName("zhang" + i);
            user.setPassword("zhang" + i);
            userList.add(user);
        }

        List<UserBO> userBOList = copyList(userList, UserBO.class);
        for (UserBO userBO : userBOList) {
            System.out.println(userBO);
        }
    }
}

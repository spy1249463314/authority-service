package com.sunpy.permissionservice.service;

import com.sunpy.permissionservice.po.UserGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
public interface IUserGroupService extends IService<UserGroup> {

}

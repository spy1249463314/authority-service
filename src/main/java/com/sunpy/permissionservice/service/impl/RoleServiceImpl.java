package com.sunpy.permissionservice.service.impl;

import com.sunpy.permissionservice.po.Role;
import com.sunpy.permissionservice.dao.RoleMapper;
import com.sunpy.permissionservice.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}

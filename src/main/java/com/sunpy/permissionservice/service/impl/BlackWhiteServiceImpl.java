package com.sunpy.permissionservice.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.commonservice.model.ResultModel;
import com.sunpy.commonservice.util.SnowFlakeIdUtil;
import com.sunpy.commonservice.util.TimeUtil;
import com.sunpy.permissionservice.po.BlackWhite;
import com.sunpy.permissionservice.dao.BlackWhiteMapper;
import com.sunpy.permissionservice.service.IBlackWhiteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sunpy.permissionservice.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunpy
 * @since 2022-10-276
 */
@Service
public class BlackWhiteServiceImpl extends ServiceImpl<BlackWhiteMapper, BlackWhite> implements IBlackWhiteService {

    @Value("${machine.workerId}")
    private long workerId;

    @Value("${machine.datacenterId}")
    private long datacenterId;

    @Autowired
    private BlackWhiteMapper blackWhiteMapper;


    @Override
    public List<BlackWhite> selectUserIdByUsername(String username) throws CommonException {

        return blackWhiteMapper.selectUserIdByUsername(username, (long) Constants.STATUS_STOP);
    }

    @Override
    public ResultModel<String> insertList(BlackWhite blackWhite) {
        if (Objects.isNull(blackWhite)) {
            throw new CommonException("blackWhite对象不能为空");
        }

        long id = new SnowFlakeIdUtil(workerId, datacenterId).genNextId();
        blackWhite.setBlackWhiteId(id);
        blackWhite.setStatus((long) Constants.STATUS_USING);
        blackWhite.setCreateTime(TimeUtil.getLocalDateTime());
        blackWhite.setUpdateTime(TimeUtil.getLocalDateTime());
        int i = blackWhiteMapper.insert(blackWhite);
        ResultModel<String> resultModel = new ResultModel<>();
        resultModel.setTime(TimeUtil.getNowTime());

        if (i != 1) {
            resultModel.setCode(500);
            resultModel.setSuccess(false);
            resultModel.setMsg("插入黑白名单失败");
        }

        resultModel.setMsg("插入黑白名单成功");
        return resultModel;
    }

    @Override
    public ResultModel<String> removeList(BlackWhite blackWhite) {
        if (Objects.isNull(blackWhite)) {
            throw new CommonException("blackWhite对象不能为空");
        }

        UpdateWrapper<BlackWhite> wrapper = new UpdateWrapper<BlackWhite>();

        if (Objects.isNull(blackWhite.getUserId())) {
            wrapper.eq("user_id", blackWhite.getUserId());
        }

        if (StrUtil.isNotBlank(blackWhite.getUserIp())) {
            wrapper.eq("user_ip", blackWhite.getUserIp());
        }

        if (Objects.isNull(blackWhite.getUserId())) {
            wrapper.eq("user_id", blackWhite.getUserId());
        }

        if (StrUtil.isNotBlank(blackWhite.getJwtCode())) {
            wrapper.eq("jwt_code", blackWhite.getJwtCode());
        }

        if (StrUtil.isNotBlank(blackWhite.getUserIp())) {
            wrapper.eq("user_ip", blackWhite.getUserIp());
        }

        if (Objects.isNull(blackWhite.getStatus())) {
            blackWhite.setStatus((long) Constants.STATUS_STOP);
        }

        int i = blackWhiteMapper.update(blackWhite, wrapper);

        ResultModel<String> resultModel = new ResultModel<>();
        resultModel.setTime(TimeUtil.getNowTime());

        if (i != 1) {
            resultModel.setCode(500);
            resultModel.setSuccess(false);
            resultModel.setMsg("删除黑名单上的记录失败");
        }

        resultModel.setMsg("删除黑名单上的记录成功");
        return resultModel;
    }

    @Override
    public ResultModel<List<BlackWhite>> queryByWrapper(BlackWhite blackWhite) {
        QueryWrapper wrapper = new QueryWrapper();

        if (Objects.isNull(blackWhite.getUserId())) {
            wrapper.eq("user_id", blackWhite.getUserId());
        }

        if (StrUtil.isNotBlank(blackWhite.getUserIp())) {
            wrapper.eq("user_ip", blackWhite.getUserIp());
        }

        if (Objects.isNull(blackWhite.getUserId())) {
            wrapper.eq("user_id", blackWhite.getUserId());
        }

        if (StrUtil.isNotBlank(blackWhite.getJwtCode())) {
            wrapper.eq("jwt_code", blackWhite.getJwtCode());
        }

        if (StrUtil.isNotBlank(blackWhite.getUserIp())) {
            wrapper.eq("user_ip", blackWhite.getUserIp());
        }

        wrapper.eq("status", Constants.STATUS_STOP);
        List<BlackWhite> blackWhiteList = blackWhiteMapper.selectList(wrapper);
        ResultModel<List<BlackWhite>> resultModel = new ResultModel<>();
        resultModel.setTime(TimeUtil.getNowTime());
        resultModel.setMsg("查询黑名单上的记录成功");
        resultModel.setRes(blackWhiteList);
        return resultModel;
    }
}

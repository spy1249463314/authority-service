package com.sunpy.permissionservice.service.impl;

import com.sunpy.permissionservice.po.UserGroup;
import com.sunpy.permissionservice.dao.UserGroupMapper;
import com.sunpy.permissionservice.service.IUserGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Service
public class UserGroupServiceImpl extends ServiceImpl<UserGroupMapper, UserGroup> implements IUserGroupService {

}

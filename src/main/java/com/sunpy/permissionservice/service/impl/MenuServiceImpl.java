package com.sunpy.permissionservice.service.impl;

import com.sunpy.permissionservice.po.Menu;
import com.sunpy.permissionservice.dao.MenuMapper;
import com.sunpy.permissionservice.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

}

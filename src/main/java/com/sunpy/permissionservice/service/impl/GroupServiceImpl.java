package com.sunpy.permissionservice.service.impl;

import com.sunpy.permissionservice.po.Group;
import com.sunpy.permissionservice.dao.GroupMapper;
import com.sunpy.permissionservice.service.IGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements IGroupService {

}

package com.sunpy.permissionservice.service.impl;

import com.sunpy.permissionservice.po.RolePermission;
import com.sunpy.permissionservice.dao.RolePermissionMapper;
import com.sunpy.permissionservice.service.IRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements IRolePermissionService {

}

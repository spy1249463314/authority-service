package com.sunpy.permissionservice.service;

import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.commonservice.model.ResultModel;
import com.sunpy.permissionservice.po.BlackWhite;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sunpy
 * @since 2022-10-276
 */
public interface IBlackWhiteService extends IService<BlackWhite> {

    ResultModel<String> insertList(BlackWhite blackWhite) throws CommonException;

    ResultModel<String> removeList(BlackWhite blackWhite) throws CommonException;

    ResultModel<List<BlackWhite>> queryByWrapper(BlackWhite blackWhite) throws CommonException;

    List<BlackWhite> selectUserIdByUsername(String username) throws CommonException;

}

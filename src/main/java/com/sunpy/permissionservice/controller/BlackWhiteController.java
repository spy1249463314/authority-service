package com.sunpy.permissionservice.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sunpy
 * @since 2022-10-276
 */
@RestController
@RequestMapping("/black-white")
public class BlackWhiteController {

}

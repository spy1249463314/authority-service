package com.sunpy.permissionservice.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@RestController
@RequestMapping("/role")
public class RoleController {

}

package com.sunpy.permissionservice.dao;

import com.sunpy.permissionservice.po.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}

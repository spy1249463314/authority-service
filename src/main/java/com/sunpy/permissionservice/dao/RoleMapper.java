package com.sunpy.permissionservice.dao;

import com.sunpy.permissionservice.po.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 根據用戶id查詢角色信息
     * @param userId
     * @return
     */
    List<Role> selectRolesByUserId(@Param("userId") Long userId);
}

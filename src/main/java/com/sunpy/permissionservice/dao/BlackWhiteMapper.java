package com.sunpy.permissionservice.dao;

import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.permissionservice.po.BlackWhite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sunpy
 * @since 2022-10-276
 */
@Mapper
public interface BlackWhiteMapper extends BaseMapper<BlackWhite> {

    List<BlackWhite> selectUserIdByUsername(@Param("username") String username,
                                                   @Param("status") Long status) throws CommonException;
}

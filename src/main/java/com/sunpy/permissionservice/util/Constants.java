package com.sunpy.permissionservice.util;

public class Constants {

    // 正在使用中
    public static final int STATUS_USING = 1;
    // 已经停用
    public static final int STATUS_STOP = 2;
    // 已经过期
    public static final int STATUS_EXPIRE = 3;



}

package com.sunpy.permissionservice.util;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.keywords.MySqlKeyWordsHandler;

public class GenCodeUtil {


    public final static String url = "jdbc:mysql://49.235.73.14:3390/permission?useUnicode=true&serverTimezone=GMT&useSSL=false&characterEncoding=utf8";
    public final static String username = "root";
    public final static String password = "spy1679358426";

    public static void main(String[] args) {

        // 数据源配置
        DataSourceConfig.Builder dataSourceConfig = new DataSourceConfig
                .Builder(
                url,
                username,
                password)
                .dbQuery(new MySqlQuery())
                .typeConvert(new MySqlTypeConvert())
                .keyWordsHandler(new MySqlKeyWordsHandler());
        // 快速生成器
        FastAutoGenerator fastAutoGenerator = FastAutoGenerator.create(dataSourceConfig);

        // 全局配置
        fastAutoGenerator.globalConfig(builder -> {
            builder.author("sunpy") //设置作者
                    .commentDate("YYYY-MM-DD")
                    .dateType(DateType.TIME_PACK)//注释日期
                    .outputDir("src\\main\\java") //指定输出目录
                    .fileOverride();
                    //.disableOpenDir();
        });

        // 包配置
        fastAutoGenerator.packageConfig(builder -> {
            builder.parent("com.sunpy.permissionservice"); // 设置父包名
            builder.entity("po");
            builder.mapper("dao");
            builder.xml("xml");
            builder.service("service");

        });

        // 策略配置
        fastAutoGenerator.strategyConfig(builder -> {
            builder.addInclude("per_black_white"); // 设置需要生成的表名
            builder.addTablePrefix("per_"); // 设置过滤表前缀
            builder.disableSqlFilter();
            builder.mapperBuilder().enableMapperAnnotation();
            builder.entityBuilder().enableLombok(); //开启 lombok 模型
            builder.entityBuilder().enableTableFieldAnnotation(); //开启生成实体时生成字段注解
            builder.controllerBuilder().enableRestStyle(); //开启生成@RestController 控制器
        });

        fastAutoGenerator.templateEngine(new FreemarkerTemplateEngine());// 使用Freemarker引擎模板，默认的是Velocity引擎模板

        fastAutoGenerator.execute();
    }
}

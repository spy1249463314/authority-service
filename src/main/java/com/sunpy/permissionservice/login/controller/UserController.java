package com.sunpy.permissionservice.login.controller;


import cn.hutool.core.util.StrUtil;
import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.commonservice.model.ResultModel;
import com.sunpy.commonservice.util.HttpUtil;
import com.sunpy.commonservice.util.JwtUtil;
import com.sunpy.commonservice.util.RedisCache;
import com.sunpy.commonservice.util.TimeUtil;
import com.sunpy.permissionservice.bo.UserBO;
import com.sunpy.permissionservice.login.logout.ILogoutService;
import com.sunpy.permissionservice.login.service.IUserService;
import com.sunpy.permissionservice.service.IBlackWhiteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ILogoutService logoutService;

    @Autowired
    private IBlackWhiteService blackWhiteService;

    @Autowired
    private RedisCache redisCache;

    @PostMapping("/insert")
    public ResultModel<String> testUser(@RequestBody UserBO userBO) throws CommonException {
        return userService.insertUser(userBO);
    }

    @GetMapping("/test")
    public ResultModel<String> testLogin() throws CommonException {
        return new ResultModel<>();
    }

    /**
     * 用户第一次登录
     * @param userBO
     * @param response
     * @return
     * @throws CommonException
     */
    @PostMapping("/login")
    public ResultModel<String> login(@RequestBody(required = false) UserBO userBO,
                                     HttpServletResponse response) throws CommonException {
        log.info("进入controller控制器 :{}", userBO);
        if (Objects.isNull(userBO)) {
            return HttpUtil.getErrorResult(HttpServletResponse.SC_BAD_REQUEST, "参数对象不能为空", null);
        }

        if (StrUtil.isEmpty(userBO.getUserName()) || StrUtil.isEmpty(userBO.getPassword())) {
            return HttpUtil.getErrorResult(HttpServletResponse.SC_BAD_REQUEST, "用户名和密码不能为空", null);
        }

        ResultModel<String> resultModel = userService.login(userBO);
        response.setHeader("token", resultModel.getRes());
        return resultModel;
    }

    /**
     * 用户登出
     * @return
     * @throws CommonException
     */
    @GetMapping("/logout")
    public ResultModel<String> logout() throws CommonException {
        return logoutService.logout();
    }

    /**
     * 修改密码
     * @param userBO
     * @return
     * @throws CommonException
     */
    @GetMapping("/modify/password")
    public ResultModel<String> modifyPassword(@RequestBody UserBO userBO) throws CommonException {


        return null;
    }


    @PostMapping("/r/r1")
    public ResultModel<String> api1(HttpServletRequest request) {
        ResultModel<String> resultModel = new ResultModel<>();
        String token = request.getHeader("token");
        String userId = (String) JwtUtil.parseJWT(token).get("userId");
        long time = redisCache.getExpireTime("token:" + userId);
        resultModel.setMsg(time + "");
        return resultModel;
    }

    @PostMapping("/r/r2")
    public ResultModel<String> api2() throws CommonException {
        return new ResultModel<>();
    }

    @PostMapping("/r/r3")
    public ResultModel<String> api3() throws CommonException {
        return new ResultModel<>();
    }

    @GetMapping("/r/r4")
    public ResultModel<String> api4() throws CommonException {
        return new ResultModel<>();
    }
}

package com.sunpy.permissionservice.login.logout;

import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.commonservice.model.ResultModel;
import com.sunpy.commonservice.util.RedisCache;
import com.sunpy.commonservice.util.TimeUtil;
import com.sunpy.permissionservice.login.model.LoginUserDetailBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class LogoutServiceImpl implements ILogoutService {

    @Autowired
    private RedisCache redisCache;

    @Override
    public ResultModel<String> logout() throws CommonException {
        UsernamePasswordAuthenticationToken authentication
                = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

        LoginUserDetailBO customUserDetailBO = (LoginUserDetailBO) authentication.getPrincipal();
        Long userId = customUserDetailBO.getUserBO().getUserId();
        redisCache.deleteObject("token:" + userId);

        ResultModel<String> resultModel = new ResultModel<>();
        resultModel.setTime(TimeUtil.getNowTime());
        resultModel.setMsg("用户注销成功");
        resultModel.setRes(String.valueOf(userId));
        return resultModel;
    }
}

package com.sunpy.permissionservice.login.logout;

import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.commonservice.model.ResultModel;

public interface ILogoutService {

    ResultModel<String> logout() throws CommonException;
}

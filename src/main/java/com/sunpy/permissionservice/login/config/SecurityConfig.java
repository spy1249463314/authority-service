package com.sunpy.permissionservice.login.config;

import com.sunpy.permissionservice.login.filter.AuthorizeAccess;
import com.sunpy.permissionservice.login.filter.AuthorizeFilter;
import com.sunpy.permissionservice.login.filter.JwtAuthenticationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Autowired
    private AuthorizeAccess authorizeAccess;

    @Autowired
    private AuthorizeFilter authorizeFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        http/*.sessionManagement().sessionCreationPolicy(
                        SessionCreationPolicy.STATELESS
                )*/
                /*.antMatchers("/user/r/r1").hasAuthority("01")
                .antMatchers("/user/r/r2").hasAuthority("03")
                .antMatchers("/user/r/r3").denyAll()
                .antMatchers("/user/**").permitAll()//放行
                .anyRequest().authenticated()*/
                .authorizeRequests()
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O object) {
                        object.setAccessDecisionManager(authorizeAccess);
                        object.setSecurityMetadataSource(authorizeFilter);
                        return object;
                    }
                })
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin().permitAll()//表单提交放行
                .and()
                .csrf().disable()
                .sessionManagement().maximumSessions(1)
        ;
    }

    /**
     * 注册解码器
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 暴露AuthenticationManager这个Bean
     * @return
     * @throws Exception
     */
    @Bean
    public AuthenticationManager getAuthenticationManager() throws Exception {
        return super.authenticationManagerBean();
    }
}

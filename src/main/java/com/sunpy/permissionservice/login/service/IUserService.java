package com.sunpy.permissionservice.login.service;

import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.commonservice.model.ResultModel;
import com.sunpy.permissionservice.bo.UserBO;
import com.sunpy.permissionservice.login.model.LoginUserDetailBO;
import com.sunpy.permissionservice.po.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
public interface IUserService extends IService<User> {

    ResultModel<String> insertUser(UserBO userBO) throws CommonException;

    ResultModel<String> login(UserBO userBO) throws CommonException;

    ResultModel<LoginUserDetailBO> selectAllByUser(String username) throws CommonException;
}

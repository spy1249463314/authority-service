package com.sunpy.permissionservice.login.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sunpy.commonservice.exception.CommonException;
import com.sunpy.permissionservice.login.model.LoginUserDetailBO;
import com.sunpy.permissionservice.bo.MenuBO;
import com.sunpy.permissionservice.bo.RoleBO;
import com.sunpy.permissionservice.bo.UserBO;
import com.sunpy.permissionservice.dao.MenuMapper;
import com.sunpy.permissionservice.dao.RoleMapper;
import com.sunpy.permissionservice.dao.UserMapper;
import com.sunpy.permissionservice.login.service.IUserService;
import com.sunpy.permissionservice.po.Menu;
import com.sunpy.permissionservice.po.Role;
import com.sunpy.permissionservice.po.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class LoginUserDetailsImpl implements UserDetailsService {

    @Autowired
    private IUserService userService;

    /**
     * 重写loadUserByUsername方法
     * 查询自己数据库中用户信息
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userService.selectAllByUser(username).getRes();
    }



}

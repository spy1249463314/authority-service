package com.sunpy.permissionservice.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Getter
@Setter
@TableName("per_user_group")
public class UserGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("user_group_id")
    private Long userGroupId;

    @TableField("user_id")
    private Long userId;

    @TableField("group_id")
    private Long groupId;


}

package com.sunpy.permissionservice.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author sunpy
 * @since 2022-09-260
 */
@Getter
@Setter
@TableName("per_menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("menu_id")
    private Long menuId;

    @TableField("menu_code")
    private String menuCode;

    @TableField("parent_id")
    private Long parentId;

    @TableField("menu_name")
    private String menuName;

    @TableField("menu_desc")
    private String menuDesc;

    @TableField("menu_url")
    private String menuUrl;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;


}

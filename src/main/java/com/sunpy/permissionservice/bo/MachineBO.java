package com.sunpy.permissionservice.bo;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ToString
@Data
@Component
@ConfigurationProperties(prefix = "machine")
public class MachineBO {

    private Long datacenterId;

    private Long workerId;

}

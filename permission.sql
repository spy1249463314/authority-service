/*
 Navicat Premium Data Transfer

 Source Server         : 49.235.73.14-master
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : 49.235.73.14:3390
 Source Schema         : permission

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 04/10/2022 15:42:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for per_black_white
-- ----------------------------
DROP TABLE IF EXISTS `per_black_white`;
CREATE TABLE `per_black_white`  (
  `black_white_id` bigint(0) NOT NULL,
  `user_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `user_id` bigint(0) NULL DEFAULT NULL,
  `jwt_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` int(0) NOT NULL COMMENT '1使用 2停用 3过期',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`black_white_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_black_white
-- ----------------------------

-- ----------------------------
-- Table structure for per_group
-- ----------------------------
DROP TABLE IF EXISTS `per_group`;
CREATE TABLE `per_group`  (
  `group_id` bigint(0) UNSIGNED NOT NULL,
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `group_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_group
-- ----------------------------

-- ----------------------------
-- Table structure for per_menu
-- ----------------------------
DROP TABLE IF EXISTS `per_menu`;
CREATE TABLE `per_menu`  (
  `menu_id` bigint(0) UNSIGNED NOT NULL,
  `menu_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `parent_id` bigint(0) UNSIGNED NULL DEFAULT NULL,
  `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `menu_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `menu_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_menu
-- ----------------------------
INSERT INTO `per_menu` VALUES (13500418055962624, '01', 0, '用户管理', '/user/r/r1', '', '2022-09-20 22:05:35', '2022-09-20 22:05:37');
INSERT INTO `per_menu` VALUES (13500418055962634, '02', 0, '机构管理', '/user/r/r2', '', '2022-09-28 23:15:49', '2022-09-28 23:15:51');

-- ----------------------------
-- Table structure for per_role
-- ----------------------------
DROP TABLE IF EXISTS `per_role`;
CREATE TABLE `per_role`  (
  `role_id` bigint(0) UNSIGNED NOT NULL,
  `role_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_role
-- ----------------------------
INSERT INTO `per_role` VALUES (13700418055961000, '01', '角色1', '2022-09-28 23:13:33', '2022-09-28 23:13:35');
INSERT INTO `per_role` VALUES (13700418055962000, '02', '角色2', '2022-09-28 23:13:59', '2022-09-28 23:14:01');

-- ----------------------------
-- Table structure for per_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `per_role_permission`;
CREATE TABLE `per_role_permission`  (
  `role_permission_id` bigint(0) UNSIGNED NOT NULL,
  `role_id` bigint(0) UNSIGNED NOT NULL,
  `permission_id` bigint(0) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_role_permission
-- ----------------------------
INSERT INTO `per_role_permission` VALUES (11200418055962345, 13700418055961000, 13500418055962624);
INSERT INTO `per_role_permission` VALUES (11200418055963456, 13700418055962000, 13500418055962634);

-- ----------------------------
-- Table structure for per_user
-- ----------------------------
DROP TABLE IF EXISTS `per_user`;
CREATE TABLE `per_user`  (
  `user_id` bigint(0) UNSIGNED NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  `update_time` datetime(0) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_user
-- ----------------------------
INSERT INTO `per_user` VALUES (13700418055962624, 'zhangsan', '$2a$10$cXqSUY0B6/BZZCaeHmh3u.aGXZoYUN2Wjzuo18GB6NFVxKmQBRh5i', 'machine-0-0', '2022-09-17 14:32:19', '2022-09-17 14:32:19');

-- ----------------------------
-- Table structure for per_user_group
-- ----------------------------
DROP TABLE IF EXISTS `per_user_group`;
CREATE TABLE `per_user_group`  (
  `user_group_id` bigint(0) UNSIGNED NOT NULL,
  `user_id` bigint(0) UNSIGNED NOT NULL,
  `group_id` bigint(0) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_user_group
-- ----------------------------

-- ----------------------------
-- Table structure for per_user_role
-- ----------------------------
DROP TABLE IF EXISTS `per_user_role`;
CREATE TABLE `per_user_role`  (
  `user_role_id` bigint(0) UNSIGNED NOT NULL,
  `user_id` bigint(0) UNSIGNED NOT NULL,
  `role_id` bigint(0) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of per_user_role
-- ----------------------------
INSERT INTO `per_user_role` VALUES (13700411255961000, 13700418055962624, 13700418055961000);
INSERT INTO `per_user_role` VALUES (13711411255961000, 13700418055962624, 13700418055962000);

SET FOREIGN_KEY_CHECKS = 1;
